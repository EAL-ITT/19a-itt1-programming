

Sort sol. Det er navnet på et af Danmarks mest fascinerende naturfænomener. Og det kan opleves i det sydlige Jylland lige nu.

I denne tid flokkes nemlig stære fra alle landene omkring Østersøen i det sydlige Jylland, inden de i store flokke
flyver videre for at overvintre i England og langs den nordvesteuropæiske kyst

På de flade, fugtige græsmarker i Sydjylland æder stærene biller og samler energi til den videre rejse.
I timen efter solnedgang går de til ro i tagrørene

Stærene holder sammen i store flokke på op til 400.000 stære. Enkelte flokke kan være på en million stære.
Når de mange tusinde stære flyver i flok i solnedgangen danner de fantastiske formationer, som formørker solen,
og som derfor kaldes sort sol

Sort sol kan opleves fra august og nogle gange helt til december.
Til februar begynder stærene at komme tilbage, og sort sol kan opleves igen fra februar til midt i april

Sort sol er en stor attraktion i Danmark. Og hvert forår og efterår rejser mange turister til Syd- og
Sønderjylland for at opleve sort sol

Måske kender du også sort sol fra musikkens verden? Sort Sol er nemlig også navnet på et dansk rockband.

Det er svært at få arbejde i Danmark. Og hvis du hverken taler dansk eller engelsk, er det nærmest umuligt.
Det bekræfter en ny undersøgelse.

854 danske virksomheder har deltaget i undersøgelsen og svaret på spørgsmålet:
Er det muligt at få job i jeres virksomhed, hvis man hverken taler dansk eller engelsk?
94 procent svarer, at det er svært eller umuligt.

Hvis man taler engelsk, er det lidt nemmere. De 854 virksomheder har også svaret på spørgsmålet:
Er det muligt at få job i jeres virksomhed, hvis man taler engelsk, men ikke dansk.
Her svarer 43 procent af virksomhederne, at det er muligt, mens 57 procent siger, at det er svært eller umuligt.

Hvis man taler engelsk, men ikke dansk, er chancen for at få arbejde størst i Region Hovedstaden.
Her svarer 50 procent af virksomhederne, at det er muligt at få job, hvis man taler engelsk, men ikke dansk.
I Region Nordjylland er kun 39 procent af arbejdsgiverne åbne over for arbejdskraft, der taler engelsk, men ikke dansk.

Hvis man hverken taler dansk eller engelsk, er chancen for at få arbejde størst i Region Syddanmark.
Her siger 9 procent af virksomhederne, at det er muligt at få job, hvis man hverken taler dansk eller engelsk.
Kun 4 procent af virksomhederne i Region Hovedstaden og i Region Sjælland er åbne over for arbejdskraft,
der hverken taler dansk eller engelsk.

Der er også branche-forskelle:

Hvis man taler engelsk, men ikke dansk, er chancerne størst i medicinalbranchen, energisektoren og bygge- og anlægsbranchen.

Hvis man hverken taler dansk eller engelsk, er chancerne størst i bygge- og anlægsbranchen og i detailbranchen (supermarkeder).

Hvad med dig? Har du job i Danmark? Hvad laver du? Hvor arbejder du? Søger du job? Hvor vil du gerne arbejde? Taler du engelsk – og/eller dansk?