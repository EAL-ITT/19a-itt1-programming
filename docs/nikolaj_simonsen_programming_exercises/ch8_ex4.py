"""
Exercise 4: Download a copy of the file www.py4e.com/code3/romeo.txt.
Write a program to open the file romeo.txt and read it line by line. For
each line, split the line into a list of words using the split function.
For each word, check to see if the word is already in a list. If the word
is not in the list, add it to the list. When the program completes, sort
and print the resulting words in alphabetical order.

Enter file: romeo.txt
['Arise', 'But', 'It', 'Juliet', 'Who', 'already',
'and', 'breaks', 'east', 'envious', 'fair', 'grief',
'is', 'kill', 'light', 'moon', 'pale', 'sick', 'soft',
'sun', 'the', 'through', 'what', 'window',
'with', 'yonder']
"""

fhand = open('./files/romeo.txt')
word_list = []

for line in fhand:
    #print(line)
    words = line.split()
    for word in words:
        # word = word.title() # For each first letter capitalized
        if word not in word_list:
            # word_list.append(word.lower()) # for real alphabetic order
            word_list.append(word) # as the exercise
        else:
            continue
word_list.sort()
print(word_list)
#    print('Debug:', words)
#    if len(words) < 2 or words[0] != 'From':
#        continue
#    print(words[2])