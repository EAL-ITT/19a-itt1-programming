---
Week: 45
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 45 19A-ITT1-programming - Lists

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 8 exercises using pair programming 
* Read chapter 9 in Python For Everybody
* Work on your own challenge

### Learning goals

The student can:

* Implement Lists
* Nest lists
* Traverse lists
* Use list operations (+, *, slice)
* Use list functions
* Mutate lists 
* Use lists in functions

The student knows: 

* The list type
* When to use a list
* List methods
* List reference vs. list copy

## Deliverables

* Chapter 8 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Friday 2019-11-07 (B class) Thursday 2019-11-08 (A class)

* 8:15 Introduction to the day  
* 9:00 Student presentations  
    2 groups of 2 students shows how they solved chapter 7 exercises from py4e
* 9:30 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Solve Chapter 8 exercises using pair programming   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 9 in Python For Everybody 
* 13:30 Q&A session about your own challenges
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

[pycharm virtual environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

PY4E Chapter 9 video lessons:

* [part 1](https://youtu.be/yDDRMb-1cxI)
* [part 2](https://youtu.be/LRSIuH94XM4)
* [part 3](https://youtu.be/ZDjiFB1Ib84)