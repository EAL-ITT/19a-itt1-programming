---
Week: 48
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 48 19A-ITT1-programming - Regular Expressions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 11 exercises 
* Read chapter 12 in Python For Everybody
* Test Python programs presented in chapter 12 (not the exercises)

### Learning goals

The student can:

* Use regular expressions

The student knows: 

* Basic syntax of regular expressions
* When to use regular expressions

## Deliverables

* Chapter 11 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Friday 2019-11-21 (B class) Thursday 2019-11-22 (A class)

* 8:15 Introduction to the day 
    * Evaluation Survey feedback
* 9:00 Student presentations  
    2 groups of 2 students shows how they solved chapter 10 exercises from py4e
* 9:30 Hands-on time: Exercises

    Things are beginning to get interesting, you can already do a lot in python and the next few weeks will be about interacting with the web.  
    Needless to say it is an important skill to have as a programmer working within the field of IoT and the exercises today is a little different.

* 11:30 Lunch break
* 12:15 Hands-on time: Continue exercises  
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

RegEx online parser and bulder:

* [regex101.com](https://regex101.com/)

PY4E Chapter 12 video lessons:

* [part 1](https://youtu.be/RsnaRPC52G0)
* [part 2](https://youtu.be/Bvx7vY454xw)
* [part 3](https://youtu.be/Lr9Vm-VghAk)
* [part 4](https://youtu.be/-cmlmaVSONg)
* [part 5](https://youtu.be/k1sUxGPpQOk)
* [part 6](https://youtu.be/D7ZI8--qbBw)

PY4E Chapter 12 worked examples:
* [socket](https://youtu.be/EqUyu8ZZYUE)  
* [urllib](https://youtu.be/jKaCKIdIoks)  
* [beautifulsoup](https://youtu.be/mhaHWiSPxxE)  
