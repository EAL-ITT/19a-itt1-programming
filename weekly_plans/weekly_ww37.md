---
Week: 37/38
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 37/38 19A-ITT1-programming - Basics

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 3 in Python For Everybody

### Learning goals

The student can:

* Perform daily GIT operations

The student knows:

* Programming basics
    * commenting code
    * input and output
    * value types
    * operators
    * variables
    * statements
    * reserved words
    * mnemonic naming
    * concatenation

## Deliverables

* Chapter 2 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Thursday 2019-09-12 (B class) and Friday 2019-09-20 (A class)

* 8:15 Introduction to the day 
* 8:30 Student presentations  
    2 groups of 2 students shows how they solved chapter 1 exercises from py4e
* 9:15 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Chapter 2 exercises   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 3 in Python For Everybody 
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

Socratica videos covering some of the topics from chapter 2 and 3:

* [Python strings](https://youtu.be/iAzShkKzpJo)
* [Numbers in Python Version 3](https://youtu.be/_87ASgggEg0)
* [Arithmetic in Python V3](https://youtu.be/Aj8FQRIHJSc)

PY4E Chapter 3 video lessons:

* [part 1](https://youtu.be/2aA3VBdcl6A)
* [part 2](https://youtu.be/OczkNrHPBps)