---
Week: 40/41
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 40/41 19A-ITT1-programming - Iteration

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 6 in Python For Everybody

### Learning goals

The student can implement:

* Iteration
* Loops
* While
* For

The student knows:

* Loop patterns
* Debugging by bisection

## Deliverables

* Chapter 5 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Thursday 2019-10-03 (B class) and Friday 2019-10-11 (A class)

* 8:15 Introduction to the day   

* 8:30 Student presentations  
    2 groups of 2 students shows how they solved chapter 4 exercises from py4e
* 9:15 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Solve Chapter 5 exercises using pair programming   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 6 in Python For Everybody 
* 13:30 Q&A session with Nikolaj
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 6 video lessons:

* [part 1](https://youtu.be/dr98iM4app8)
* [part 2](https://youtu.be/bIFpJ-qZ3Cc)