---
Week: 36
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 36 19A-ITT1-programming - Introduction

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Python installed and tested
* Read chapter 1 in Python For Everybody
* Read chapter 2 in Python For Everybody
* Python For Everybody - Chapter 1 exercises completed 

### Learning goals

The student knows:

* What a program is
* What Python REPL is and how to use it
* What Python IDLE is and how to use it
* How to document using Gitlab

## Deliverables

* Gitlab exercises project created
* Chapter 1 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Thursday 2019-09-05 (B class) and Friday 2019-09-06 (A class)

* 8:15 Introduction to the course

    * [Lecture plan](https://eal-itt.gitlab.io/19a-itt1-programming/)  
    * 19A_ITT1_programming_lecture_plan.pdf)  
    * Cooperative learning  
    * [Gitlab pages](https://eal-itt.gitlab.io/19a-itt1-programming/)  
    * [Python For Everybody](https://www.py4e.com/book.php)  
    * [Socratica videos](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-) 
    * Expectations

* 8:45 Exercise 0
* 9:45 Coffee break
* 10:00 Exercise 1
* 11:30 Lunch break
* 12:15 Buddy work

    * Read chapter 1 and 2 in Python For Everybody.  
    * Complete Python For Everybody, chapter 1 exercises.
        * Document each exercise in seperate .py files on gitlab

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming) for details.

* Exercise 0: Install Python + Hello World
    
* Exercise 1: GIT and Gitlab

    * Follow the instructions in the [gitlab daily workflow](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0) codelab  


## Comments

* this weeks presentation [ww36-slides-19a-itt1-programming](https://docs.google.com/presentation/d/1FZBefr86lhkdRMllQW0O1rFayuVgZAdmB1cMURsVB5Y/edit?usp=sharing)

PY4E Chapter 2 video lessons:

[part 1](https://youtu.be/7KHdV6FSpo8)
[part 2](https://youtu.be/kefrGMAglGs)