
# Additional resources

## Communication

* [Weekly plans, exercises, lecture plan](https://eal-itt.gitlab.io/19a-itt1-programming/)

* Exercises hand-in: Your gitlab projects, remember to include @npes

* Not able to attend class - write to nisi@ucl.dk 

* Everything else https://ucl.itslearning.com 

**I will be on your discord servers, but this is not an official channel of communication**

## Expectations

* Show up on time
* Notify your buddy and nisi if you are unable to attend
* Complete all reading, exercises and OLA’s
* Practice, practice, practice
* Be curious

## Sample code from Charles Severance (PY4E Author)

[Everything in .zip file](https://www.py4e.com/code3.zip)  
[Individual files](https://www.py4e.com/code3/)

## Sample code from Nikolaj Simonsen (incomplete)

[nikolaj_simonsen_programming_exercises](https://gitlab.com/EAL-ITT/19a-itt1-programming/tree/master/docs/nikolaj_simonsen_programming_exercises)