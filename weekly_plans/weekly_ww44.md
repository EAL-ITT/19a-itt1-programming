---
Week: 44
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 44 19A-ITT1-programming - Files

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 7 exercises using pair programming 
* Read chapter 8 in Python For Everybody

### Learning goals

The student can:

* Open/read/write/close files
* Parse files
* Handle file exceptions 

## Deliverables

* Chapter 7 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Friday 2019-11-01 (A and B class)

* 8:15 Introduction to the day  
    * Number guessing game - bonus challenges ? Coffee ?
    * PyCharm - [setup a project](https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html)
    * [Survey](https://docs.google.com/forms/d/e/1FAIpQLSfxwWR33z3BYkE27P_d5PUzduWIHGQ8nFaQY5q4v-dEBC7_rA/viewform) 

* 9:00 Student presentations  
    2 groups of 2 students shows how they solved chapter 6 exercises from py4e
* 9:30 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Solve Chapter 7 exercises using pair programming   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 8 in Python For Everybody 
* 13:30 Q&A session with Nikolaj
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

[pycharm virtual environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

PY4E Chapter 8 video lessons:

* [part 1](https://youtu.be/ljExWqnWQvo)
* [part 2](https://youtu.be/bV1FQUBIApM)
* [part 3](https://youtu.be/GxADdpo6EP4)